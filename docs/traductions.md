# Traductions
<a href="https://weblate.framasoft.org/engage/funky-framadate/">
<img src="https://weblate.framasoft.org/widgets/funky-framadate/-/funky-framadate-front/svg-badge.svg" alt="État de la traduction" />
</a>
<a href="https://weblate.framasoft.org/engage/funky-framadate/">
<img src="https://weblate.framasoft.org/widgets/funky-framadate/-/funky-framadate-front/multi-auto.svg" alt="État de la traduction" />
</a>

Les chaînes sont traduites via des clés de fichiers JSON
exemple: [src/assets/i18n/FR.json]()

La mise en place de weblate pour faire des traductions collaboratives a été faite.
Vous pouvez contribuer ici https://weblate.framasoft.org/projects/funky-framadate/funky-framadate-front

Nous avons récupéré les chaînes de traduction du projet Framadate v1, il faudra faire un nettoyage des clés et valeurs inutilisées dans les fichiers de traduction.
# Ajouter de nouvelles langues

Le Weblate effectue automatiquement des Merge Request vers Framagit, si une langue est nouvelle elle proposera de créer un fichier **nom_de_la_langue.json** dans le dossier **src/assets/i18n**

Pour que l'appli reconnaisse ces nouvelles langues il faut les enregistrer dans l'AppModule.
Chaque fichier de traduction est automatiquement chargé dans le [AppModule](../../src/app/app.module.ts) avec le Loader de @ngx-translate. Il faut ensuite ajouter le code de la langue dans l'énumération du fichier **/app/core/enums/language.enum.ts** Examinez l'exemple pour rajouter votre propre traduction.

``` typescript
export enum Language {
	FR = 'fr',
	BR = 'br',
	EN = 'en',
	//... etc
	}

```

# prise en charges des traductions manquantes
Les textes manquants seront indiqués avec "MISSING TRANSLATION FOR" et le nom de la clé demandée par le HTML.

# LIBRARIES USED

|     status      | lib choice_label                                                       | usage                                                     |
| :-------------: | -------------------------------------------------------------- | --------------------------------------------------------- |
|                 | [axios](https://github.com/axios/axios)                        | http client                                               |
|                 | [bulma](https://bulma.io/)                                     | CSS framework                                             |
|                 | [chart.js](https://www.chartjs.org/)                           | PrimeNG solution for graphs. (Chart.js installs MomentJS) |
|                 | [compodoc](https://compodoc.app/)                              | Generate technic documentation                            |
|                 | ESlint, Prettier, Lint-staged                                  | Format & lint code                                        |
|                 | [fork-awesome](https://forkaweso.me)                           | Icons collection                                          |
|                 | [fullcalendar](https://fullcalendar.io/docs/initialize-es6)    | PrimeNG solution to manage & display calendars            |
|                 | [husky](https://www.npmjs.com/package/husky)                   | Hook actions on commit                                    |
|                 | [jest](https://jestjs.io/)                                     | test engine                                               |
|                 | [json-server](https://www.npmjs.com/package/json-server)       | local server for mocking data backend                     |
|     removed     | [locale-enum](https://www.npmjs.com/package/locale-enum)       | enum of all locales                                       |
|                 | [momentJS](https://momentjs.com/)                              | manipulate dates. (chartJS’s dependency)                  |
| to be installed | [ng2-charts](https://valor-software.com/ng2-charts/)           | Manipulate graphs along with chart.js                     |
|                 | [ngx-clipboard](https://www.npmjs.com/package/ngx-clipboard)   | Handle clipboard                                          |
|                 | [ngx-markdown](https://www.npmjs.com/package/ngx-markdown)     | markdown parser                                           |
|                 | [ngx-webstorage](https://www.npmjs.com/package/ngx-webstorage) | handle localStorage & webStorage                          |
|                 | [primeNG](https://www.primefaces.org/primeng/)                 | UI components collection                                  |
|                 | [quill](https://www.npmjs.com/package/quill)                   | powerful rich text editor. WYSIWYG.                       |
| to be installed | [short-uuid](https://www.npmjs.com/package/short-uuid)         | generate uuid                                             |
|     removed     | [storybook](https://storybook.js.org/)                         | StyleGuide UI                                             |
|                 | [ts-mockito](https://www.npmjs.com/package/ts-mockito)         | Mocks for testing.                                        |
|  to be removed  | [uuid](https://www.npmjs.com/package/uuid)                     | generate uuid                                             |

---

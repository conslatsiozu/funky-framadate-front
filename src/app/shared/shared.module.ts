import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatStepperModule } from '@angular/material/stepper';
import { TranslateModule } from '@ngx-translate/core';
import { NgChartsModule } from 'ng2-charts';

import { ChoiceDetailsComponent } from './components/choice-details/choice-details.component';
import { CommentsComponent } from './components/comments/comments.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { LanguageSelectorComponent } from './components/selectors/language-selector/language-selector.component';
import { ThemeSelectorComponent } from './components/selectors/theme-selector/theme-selector.component';
import { SettingsComponent } from './components/settings/settings.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { CopyTextComponent } from './components/ui/copy-text/copy-text.component';
import { ErasableInputComponent } from './components/ui/erasable-input/erasable-input.component';
import { WipTodoComponent } from './components/ui/wip-todo/wip-todo.component';
import { ErrorsListComponent } from '../features/shared/components/ui/form/errors-list/errors-list.component';
import { ShortcutsHelpComponent } from '../features/shared/components/ui/shortcuts-help/shortcuts-help.component';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { AboutComponent } from '../features/shared/components/ui/static-pages/about/about.component';
import { RouterModule } from '@angular/router';
import { ChoiceButtonDinumComponent } from '../features/shared/components/choice-button-dinum/choice-button-dinum.component';
import { DialogModule } from 'primeng/dialog';
import { HeaderComponent } from '../core/components/header/header.component';
import { FooterComponent } from '../core/components/footer/footer.component';
import { SkipLinksComponent } from 'src/app/features/shared/components/rgaa/skip-links/skip-links.component';

const COMPONENTS = [
	AboutComponent,
	ChoiceDetailsComponent,
	CommentsComponent,
	FeedbackComponent,
	LanguageSelectorComponent,
	PageNotFoundComponent,
	SettingsComponent,
	SpinnerComponent,
	ThemeSelectorComponent,
	CopyTextComponent,
	ErasableInputComponent,
	ErrorsListComponent,
	WipTodoComponent,
	ShortcutsHelpComponent,
	ChoiceButtonDinumComponent,
	HeaderComponent,
	FooterComponent,
];

const ANGULAR_MODULES = [CommonModule, NgChartsModule, FormsModule, TranslateModule];

const MATERIAL_MODULES = [
	MatButtonModule,
	MatCheckboxModule,
	MatDatepickerModule,
	MatDialogModule,
	MatFormFieldModule,
	MatInputModule,
	MatProgressSpinnerModule,
	MatSidenavModule,
	MatSlideToggleModule,
	MatSnackBarModule,
	MatStepperModule,
];

@NgModule({
	declarations: [COMPONENTS, SkipLinksComponent],
	imports: [...ANGULAR_MODULES, ...MATERIAL_MODULES, ConfirmDialogModule, RouterModule, DialogModule],
	exports: [...ANGULAR_MODULES, ...MATERIAL_MODULES, ...COMPONENTS, SkipLinksComponent],
	providers: [ConfirmationService],
})
export class SharedModule {}

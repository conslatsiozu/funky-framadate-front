import { ShortcutEventOutput } from 'ng-keyboard-shortcuts';

export const FramaKeyboardShortcuts = [
	{
		key: ['cmd + a'],
		command: (output: ShortcutEventOutput) => console.log('command + a', output),
	},
	{
		key: 'ctrl + b',
		preventDefault: true,
		command: (output: ShortcutEventOutput) => console.log('control + b', output),
	},
	{
		key: 'ctrl + plus',
		preventDefault: true,
		command: (output: ShortcutEventOutput) => console.log('control + plus key', output),
	},
];
